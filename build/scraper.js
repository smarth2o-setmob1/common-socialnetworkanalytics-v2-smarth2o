"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator.throw(value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments)).next());
    });
};
// Load system modules
const path_1 = require('path');
const fs_1 = require('fs');
const stream_1 = require('stream');
// Load modules
const initDebug = require('debug');
const twitter_scraper_1 = require('twitter-scraper');
const program = require('commander');
// Load my modules
const utils_1 = require('./utils');
// Constant declaration
const debug = initDebug('scraper');
const CWD = process.cwd();
const pkg = require(path_1.resolve(__dirname, '..', 'package.json'));
// Module variables declaration
// Module interfaces declaration
// Module functions declaration
function startScraper(query, transform, write) {
    const s = new twitter_scraper_1.Scraper(query);
    // Pipe the components
    s.pipe(transform).pipe(write);
    // Start the scraper
    s.start();
    return s;
}
function parseArguments(args) {
    return __awaiter(this, void 0, void 0, function* () {
        program
            .version(pkg.version)
            .description(pkg.description)
            .option('-M, --mongo <mongo>', 'Mongo config file')
            .option('-m, --host <host>', 'Mongo host [localhost]', 'localhost')
            .option('-p, --port <port>', 'Mongo port [27017]', 27017)
            .option('-d, --database <database>', 'Mongo database name')
            .option('-c, --collection <collection>', 'Mongo collection [tweets]', 'tweets')
            .option('-T, --twitter <twitter>', 'Twitter config file')
            .option('-k, --key <key>', 'Twitter key')
            .option('-s, --secret <secret>', 'Twitter secret')
            .option('-t, --token <token>', 'Twitter access token')
            .option('-y, --token-secret <tokenSecret>', 'Twitter access token secret')
            .option('-Q, --query-file <queryFile>', 'Twitter query file')
            .option('-q, --query <query>', 'Twitter query')
            .parse(args);
        // Define a noop transformer
        let transform = new stream_1.PassThrough({ objectMode: true });
        // Define a console writer
        let write = new utils_1.ToConsole();
        // Try to get the query
        let query = program['query'];
        if (!query && program['queryFile']) {
            const queryFile = program['queryFile'];
            debug('Using query file: %s', queryFile);
            const fullPath = path_1.resolve(CWD, queryFile);
            query = fs_1.readFileSync(fullPath, 'utf8');
        }
        debug('Query: "%s"', query);
        // Check if must use the MongoDB writer
        if (program['mongo'] || program['database']) {
            debug('Using the mongo writer');
            let config;
            if (program['mongo']) {
                debug('Using the mongo config file: %s', program['mongo']);
                config = require(path_1.resolve(CWD, program['mongo']));
            }
            else {
                debug('Using the config from the cli');
                config = {
                    host: program['host'],
                    port: program['port'],
                    database: program['database'],
                    collection: program['collection'],
                };
            }
            debug('Using the mongo config: ', config);
            const db = yield utils_1.connect(config);
            const collection = db.collection(config.collection);
            write = new utils_1.MongoWriter(collection);
            write.once('finish', () => db.close());
        }
        // Check if must use the twitter transformer
        if (program['twitter'] || program['key']) {
            debug('Using the twitter transformer');
            let config;
            if (program['twitter']) {
                debug('Using the twitter config file: %s', program['twitter']);
                config = require(path_1.resolve(CWD, program['twitter']));
            }
            else {
                config = {
                    key: program['key'],
                    secret: program['secret'],
                    token: program['token'],
                    tokenSecret: program['tokenSecret'],
                };
            }
            debug('Using the twitter config: ', config);
            transform = new utils_1.Enrich(config);
        }
        const scraper = startScraper(query, transform, write);
        scraper.on('end', () => debug('All done, got %d tweets', scraper.total));
    });
}
module.exports = parseArguments;
//  50 6F 77 65 72 65 64  62 79  56 6F 6C 6F 78
//# sourceMappingURL=scraper.js.map
"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator.throw(value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments)).next());
    });
};
// Load system modules
const url_1 = require('url');
const stream_1 = require('stream');
// Load modules
const initDebug = require('debug');
const mongodb_1 = require('mongodb');
const Twit = require('twit');
const social_post_wrapper_1 = require('@volox/social-post-wrapper');
// Load my modules
// Constant declaration
const debug = initDebug('scraper:utils');
const WINDOW = 1000 * 60 * 15;
const MAX_TWEETS = 100;
// Module functions declaration
function wait(milliseconds) {
    return new Promise(res => {
        setTimeout(res, milliseconds);
    })
        .then(() => { });
}
function connect(config) {
    return __awaiter(this, void 0, void 0, function* () {
        const url = url_1.format({
            protocol: 'mongodb',
            slashes: true,
            hostname: config.host,
            port: String(config.port),
            pathname: config.database,
        });
        debug('Connectiong to mongo @: %s', url);
        return yield mongodb_1.MongoClient.connect(url);
    });
}
exports.connect = connect;
// Module class declaration
class Enrich extends stream_1.Transform {
    constructor(credentials) {
        super({ objectMode: true });
        this.queue = [];
        this.credentials = credentials;
        const options = {
            consumer_key: credentials.key,
            consumer_secret: credentials.secret,
        };
        if (credentials.appOnly === true) {
            options.app_only_auth = true;
        }
        else {
            options.access_token = credentials.token;
            options.access_token_secret = credentials.tokenSecret;
        }
        this.api = new Twit(options);
    }
    _transform(data, enc, cb) {
        this.queue.push(data);
        if (this.queue.length >= MAX_TWEETS) {
            this.processQueue(MAX_TWEETS, cb);
        }
        else {
            return cb();
        }
    }
    _flush(cb) {
        this.processQueue(this.queue.length, cb);
    }
    processQueue(numTweets, cb) {
        const tweets = this.queue.splice(0, numTweets);
        return this.getFullTweets(tweets, cb);
    }
    getFullTweets(rawTweets, cb) {
        const ids = rawTweets.map(t => t.id);
        //const retweeters = rawTweets.map(t => {id:t.id,retweeter:t.retweeter});
        const retweeters = rawTweets.map(function(t){
            return {
                id:t.id,
                retweeter:t.retweeter
            }
        });
        return this.api
            .post('statuses/lookup', {
            id: ids.join(','),
            include_entities: true,
        })
            .then(resp => {
            const tweets = resp.data;
            for (let i = 0; i < tweets.length; i++) {
                const post = social_post_wrapper_1.default(tweets[i], 'twitter');
                post.retweeter = retweeters.find(t => t.id===post.id).retweeter;
                this.push(post);
            }
            ;
            return cb();
        })
            .catch(err => {
            debug('Twitter Error', err);
            return wait(WINDOW)
                .then(() => this.getFullTweets(rawTweets, cb));
        });
    }
}
exports.Enrich = Enrich;
class MongoWriter extends stream_1.Writable {
    constructor(collection) {
        super({ objectMode: true });
        this.collection = collection;
    }
    _write(data, enc, cb) {
        debug('Writing tweet: %s', data.id);
        return this.collection
            .insertOne(data)
            .then(() => cb())
            .catch(err => {
            debug('Mongo error', err);
            return cb();
        });
    }
}
exports.MongoWriter = MongoWriter;
class ToConsole extends stream_1.Writable {
    constructor() {
        super({ objectMode: true });
    }
    _write(data, enc, cb) {
        console.log(data);
        return cb();
    }
}
exports.ToConsole = ToConsole;
// Module initialization (at first load)
// Module exports
// Entry point
//  50 6F 77 65 72 65 64  62 79  56 6F 6C 6F 78
//# sourceMappingURL=utils.js.map